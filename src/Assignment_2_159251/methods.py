'''
Created on Oct 11, 2012

@author: jean, patison
'''
import os, shutil, SHA1, pickle

##########################################################################
# Implementation of save method. Parameter is a foldername to be backed up
# It saves all files in the foldername to myArchive/objects folder and the
# dictionary is stored in myArchive/index.txt
##########################################################################
def executesave(foldername):
    outfile   = open('myArchive'+ os.sep + 'index.txt',"w");
    for root, dir, files in os.walk(foldername):
        for name in files:
            fullname = os.path.join(root, name)
            source = fullname
            destination = 'myArchive'+ os.sep +'objects' + os.sep + name
            shutil.copyfile(source,destination)
            oldname = destination
            sig = str(SHA1.createFileSignature(fullname))
            newname = 'myArchive'+ os.sep +'objects' + os.sep + sig
            os.rename(oldname,newname)
            dict = {fullname:sig}
            pickle.dump(dict,outfile)
    print 'backup created'        
    outfile.close()

##########################################################################
# Implementation of list method. Parameter is a pattern to displayed from
# the contains of the archive.
##########################################################################
def executelist(pattern):
    filename = open("myArchive"+ os.sep +"index.txt","r")    
    try:
        while True:    
            data = pickle.load(filename)
            for k in data.keys():
                if pattern == "":
                    print k
                else:        
                    if pattern in k:
                        print k
    except EOFError:
        pass  
        
##########################################################################
# Implementation of the check method. Parameter is the name of the archive.
# The method verify the correctness of contents in the archive.
##########################################################################  
def executecheck(myArchive):
    folder = myArchive + os.sep + 'objects'
    correct = 0
    incorrect= 0
    
    for root,dir, files in os.walk(folder):
        for file in files:
            newfile = root + os.sep + file
            sig = SHA1.createFileSignature(newfile)       
            indexfile = open('myArchive'+ os.sep +'index.txt','r') 
            if sig == file and checkinindex(indexfile, sig):
                correct+=1
            else:
                incorrect+=1              
                print'error path:' + file
        
    print 'correct: '+ str(correct) + ' and incorrect: '+ str(incorrect) 
    
           
def checkinindex(indexFile, hashvalue):

    try:
        while True:
            myArchive = pickle.load(indexFile)
            for v in myArchive.values():
                if v == hashvalue:            
                    return True       
    except EOFError:
        pass         
    return False  

##########################################################################
# Implementation of the restore method. Parameter is a path to where the
# file will be restored.
##########################################################################
def executerestore(path):
    myArchive = 'myArchive' + os.sep +'objects'
    s = path
    if os.sep in path:
        nameToRecover = getName(path)
        if not nameToRecover == "":
            fileToCopy = os.path.join(myArchive,nameToRecover)
            foldername = s[0:s.rfind(os.sep)]
            filename = s[s.rfind(os.sep)+1:]
            found = False
            for root,dir, files in os.walk(foldername):
                for file in files:
                    if file == filename:
                        found = True
                        reply = raw_input('Do you want to overwrite? y/n: ')
                        if reply == "y":
                            shutil.copy(fileToCopy, path)
                            print 'file recovered to ' + path
                            
                        else:
                            print "file not recovered"   
            if not found:
                shutil.copy(fileToCopy, path)
                print 'file recovered to ' + path
        else:
            print "file not found"   
                 
    else:
        restoreFiles(path)
           
        
def getName(path): 
    indexFile = open('myArchive' + os.sep + 'index.txt','r')
    try:
        while True:     
            data = pickle.load(indexFile)
            for k in data.keys():
                if path in data:
                    return data[k]
               
    except EOFError:
        pass        
    return ""
               

def restoreFiles(simplename):
    indexFile = open('myArchive' + os.sep + 'index.txt','r')
    myArchive = 'myArchive'+ os.sep +'objects' + os.sep
    found = False
    try:
        while True:
            data = pickle.load(indexFile)
            for k in data.keys():
                if simplename in k:
                    found = True
                    sourcefile = os.path.join(myArchive,data[k])
                    name = k.replace(os.sep,'-')
                    destfile = os.path.join(myArchive + name)
                    shutil.copy(sourcefile, destfile)
                    print "file copied"
    except EOFError:
        pass
                  
    if not found:
        print 'file not found'         
