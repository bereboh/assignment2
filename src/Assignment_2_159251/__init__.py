import os, os.path, hashlib, time, methods

reply = raw_input("Enter command:  ")
print "you entered ", reply
command = reply.split(" ")

if command[1] == 'save':
    methods.executesave(command[2])

elif command[1] == 'list':
    if len(command) == 2:
        methods.executelist("") 
    else:
        methods.executelist(command[2])

elif command[1] == 'check':            
    methods.executecheck(command[2])
    
elif command[1] == 'restore':
    methods.executerestore(command[2])
    
